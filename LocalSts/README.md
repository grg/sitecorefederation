Reference:

* https://github.com/IdentityModel/Thinktecture.IdentityModel/tree/master/samples/EmbeddedSts

Installation:

* Build and run the solution. EmbeddedStsSample site will start on http://localhost:29702.
* Users can be modified in App_Data/EmbeddedStsUsers.json file.