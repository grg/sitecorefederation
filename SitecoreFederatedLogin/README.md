This solution contains a OWIN based federated login solution for sitecore. It's by no means production ready, but it might be an interesting
solution.

Reference:

* http://blog.baslijten.com/how-to-add-federated-authentication-with-sitecore-and-owin/
* https://github.com/BasLijten/SitecoreFederatedLogin

Installation:

* Install Sitecore instance with hostname http://ws
* Add the following node to your connectionstrings.config:  <add name="AuthSessionStoreContext" providerName="System.Data.SqlClient" connectionString="Data Source=.\;Initial Catalog=WSFedTokens;Integrated Security=False;User ID=sa;Password=xxxxx;"/>. It creates a new database when it's needed, login tokens will be stored in this database
* Deserialize Login.item and Logout.item files to your Sitecore instance. The files point to AuthController actions.
* Copy SitecoreOwinFederator.dll to Sitecore website's /bin folder.
* Copy App_Config/Includ/*.config files to Sitecore website's App_Config/Include folder.
* Make sure STS service is available as specified in Startup.cs (default: http://localhost:29702).
* Navigate to http://ws/login to peform federated login.
* Navigate to http://ws/logout to logout.