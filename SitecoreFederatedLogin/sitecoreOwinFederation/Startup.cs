﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.WsFederation;
using Owin;
using SitecoreOwinFederator.Authenticator;

[assembly: OwinStartup(typeof(SitecoreOwinFederator.Startup))]
namespace SitecoreOwinFederator
{
  public class Startup
  {
    public void Configuration(IAppBuilder app)
    {
      app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
      app.UseCookieAuthentication(new CookieAuthenticationOptions
      {
        SlidingExpiration = false,
        SessionStore = new SqlAuthSessionStore(new TicketDataFormat(new MachineKeyProtector())),
        TicketDataFormat = new TicketDataFormat(new MachineKeyProtector()),
        Provider = new CookieAuthenticationProvider()
      });

      app.UseWsFederationAuthentication(new WsFederationAuthenticationOptions
      {
        UseTokenLifetime = true,
        MetadataAddress = "http://localhost:29702/FederationMetadata",
        Wtrealm = "urn:multisite",
        Wreply = "http://ws/login",
      });
    }
  }
}
